<?php

use Peregrinus\RKWrapper\Http;
use Peregrinus\RKWrapper\Import\RkDiscovery;
use Peregrinus\RKWrapper\Output\AbstractOutput;

ini_set('display_errors', true);

require_once 'vendor/autoload.php';

define('URL', str_replace('/stream/rk', '', $_SERVER['REQUEST_URI']));
define('MY_URL', $_SERVER['REQUEST_SCHEME'].'//'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);


$discovery = new RkDiscovery();
$output = $discovery->getOutput();
$output->render();
