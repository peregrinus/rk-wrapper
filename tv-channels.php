<?php

$channels = yaml_parse_file('data/tv.yaml');

define('URL', $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].str_replace(basename(__FILE__), '', $_SERVER['REQUEST_URI']));

$id = 0;
foreach ($channels as $key => $channel) {
    $channels[$key]['id'] = $id;
    $channels[$key]['dataUrl'] = URL.$key.'/TV.json';
    $channels[$key]['key'] = $key;
    $id++;
}


Header('Content-Type: application/json');
echo json_encode(array_values($channels));