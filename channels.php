<?php

echo "#EXTM3U\r\n";

$cityConfig = yaml_parse_file('data/cities.yaml');
ksort($cityConfig);
foreach ($cityConfig as $city => $url) {
    $path = 'https://dev.peregrinus.de/stream/rk/'.$city.'/playlist.'.($_GET['type'] ?: 'audio');
    echo '#EXTINF:-1 channel-id="'.ucfirst($city).'"'."\r\n";
    echo $path."\r\n";

}