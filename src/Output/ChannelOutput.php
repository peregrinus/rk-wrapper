<?php

namespace Peregrinus\RKWrapper\Output;

class ChannelOutput extends AbstractOutput
{
    protected $contentType = 'application/x-mpegURL';

    public function render()
    {
        $this->contentTypeHeader();

        $path = str_replace(pathinfo(MY_URL, PATHINFO_BASENAME), '', MY_URL);
        $path .= 'playlist.'.pathinfo(MY_URL, PATHINFO_EXTENSION);

        echo "#EXTM3U\r\n";
        echo '#EXTINF:-1 channel-id="'.ucfirst($this->input->getCity()).'"'."\r\n";
        echo $path."\r\n";
    }
}