<?php

namespace Peregrinus\RKWrapper\Output;

class StreamOutput extends AbstractRSSOutput {
    protected $fileKey = 'video';
    protected $fileType = 'video/mp4';
}