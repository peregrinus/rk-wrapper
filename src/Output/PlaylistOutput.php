<?php

namespace Peregrinus\RKWrapper\Output;

use Peregrinus\RKWrapper\Import\RkImport;

class PlaylistOutput extends AbstractOutput
{

    protected $contentType = 'application/x-mpegURL';
    protected $fileKey = '';

    public function __construct(RkImport $input)
    {
        parent::__construct($input);
        $this->fileKey = pathinfo(MY_URL, PATHINFO_EXTENSION) ? pathinfo(MY_URL, PATHINFO_EXTENSION) : 'video';
    }
    public function render()
    {
        $this->contentTypeHeader();
        echo "#EXTM3U\r\n";
        foreach ($this->input->getRecords() as $record) {
            echo '#EXTINF:'.$record['seconds'].','.$record['title']."\r\n";
            echo $record[$this->fileKey]."\r\n";
        }
    }


}