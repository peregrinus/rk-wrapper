<?php

namespace Peregrinus\RKWrapper\Output;

use Peregrinus\RKWrapper\Http;
use Peregrinus\RKWrapper\Import\AbstractImport;
use Peregrinus\RKWrapper\Import\RkImport;

class TVOutput extends AbstractOutput
{

    protected $contentType = 'application/json';
    protected $fileKey = 'video';

    public function __construct(AbstractImport $input)
    {
        parent::__construct($input);
    }
    public function render()
    {
        $channels = yaml_parse_file('data/tv.yaml');
        if (!isset($channels[$this->input->getCity()])) Http::notFound();
        $channel = $channels[$this->input->getCity()];
        $channelIndex = array_search($this->input->getCity(), array_keys($channels));

        $this->contentTypeHeader();
        $videos = [];
        $id = 0;
        foreach ($this->input->getRecords() as $record) {
            $videos[] = [
                'id' => $id,
                'title' => preg_match('/(\d\d\.\d\d\.)\d\d/', $record['title'], $o) ? $o[1] : $record['title'],
                'description' => $record['description'] ?? 'Gottesdienst vom '.($record['dateString'] ?? $record['title']),
                'studio' => $channel['title'],
                'videoUrl' => $record[$this->fileKey],
                'backgroundImageUrl' => $record['thumbnail'] ?? $channel['bg'],
                'cardImageUrl' => $record['thumbnail'] ?? $channel['card'],
                'channelIndex' => $channelIndex,
                'isYouTube' => $channel['isYouTube'] ?? false,
            ];
            $id++;
        }
        echo json_encode($videos);
    }


}