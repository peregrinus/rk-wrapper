<?php

namespace Peregrinus\RKWrapper\Output;

abstract class AbstractRSSOutput extends AbstractOutput
{

    protected $fileKey = '';
    protected $fileType = '';

    public function render()
    {
        $this->contentTypeHeader();
echo '<?xml version="1.0" encoding="utf-8"?>';
?><rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:content="http://purl.org/rss/1.0/modules/content/" version="2.0">
    <channel>
        <title><?= $this->input->getTitle() ?></title>
        <link><?= MY_URL ?></link>
        <lastBuildDate><?= $this->input->getPubDate() ?></lastBuildDate>
        <pubDate><?= $this->input->getPubDate() ?></pubDate>
        <language>de</language>
        <category>Religion &amp; Spirituality / Christianity</category>

        <atom:link href="<?= MY_URL ?>" rel="self" type="application/rss+xml" />
        <?php foreach ($this->input->getRecords() as $record): ?>
            <item>
                <title><?= $record['title'] ?></title>
                <guid><?= $record[$this->fileKey].'/guid' ?></guid>
                <enclosure url="<?= $record[$this->fileKey] ?>"  length="<?= $record['length'][$this->fileKey] ?>" type="<?= $this->fileType ?>"></enclosure>
                <pubDate><?= $record['end']->format('r') ?></pubDate>
            </item>
        <?php endforeach; ?>
    </channel>
</rss>
<?php
        exit;
    }


}