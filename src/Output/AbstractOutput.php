<?php

namespace Peregrinus\RKWrapper\Output;

use Peregrinus\RKWrapper\Import\AbstractImport;
use Peregrinus\RKWrapper\Import\RkImport;

abstract class AbstractOutput
{

    protected $contentType = 'application/rss+xml';

    /** @var RkImport */
    protected $input;

    public function __construct(AbstractImport $input) {
        $this->input = $input;
    }

    public function contentTypeHeader()
    {
        Header('Content-Type: '.$this->contentType);
    }

    abstract public function render();
}