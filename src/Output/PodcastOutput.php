<?php

namespace Peregrinus\RKWrapper\Output;

class PodcastOutput extends AbstractRSSOutput
{

    protected $fileKey = 'audio';
    protected $fileType = 'audio/mp3';

}