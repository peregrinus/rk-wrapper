<?php

namespace Peregrinus\RKWrapper;

class Http
{

    public static function notFound() {
        http_response_code(404);
        die ('<h2>Nicht gefunden</h2>Das gesuchte Dokument wurde nicht auf dem Server gefunden.');
    }

    public static function head($url)
    {
        $curl = curl_init( $url );

        // Issue a HEAD request and follow any redirects.
        curl_setopt( $curl, CURLOPT_NOBODY, true );
        curl_setopt( $curl, CURLOPT_HEADER, true );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $curl, CURLOPT_USERAGENT, 'RkWrapper 1.0' );

        $data = curl_exec( $curl );
        curl_close( $curl );

        $headers = ['status' => curl_getinfo($curl, CURLINFO_HTTP_CODE)];
        if( $data ) {
            $data = explode("\r\n", $data);
            foreach ($data as $line) {
                $tmp = explode(':', $line);
                if (count($tmp) == 2) $headers[strtolower(trim($tmp[0]))] = trim($tmp[1]);
            }
        }
        return $headers;
    }
    
    public static function test($url) {
        return self::head($url)['status'] == 200;
    }
}