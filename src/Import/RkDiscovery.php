<?php

namespace Peregrinus\RKWrapper\Import;


use Peregrinus\RKWrapper\Http;

class RkDiscovery
{

    protected $city;

    protected $cityConfig;

    protected $outputClass;

    protected $channels = [];

    public function __construct() {
        $this->cityConfig = yaml_parse_file('data/cities.yaml');
        if (!$this->cityConfig) Http::notFound();

        $path = substr(URL, 0, 1) == '/' ? substr(URL, 1) : URL;
        $parts = explode('/', $path);

        if (count($parts) != 2) Http::notFound();

        $this->city = strtolower($parts[0]);

        $this->channels = yaml_parse_file('data/tv.yaml');
        if (!isset($this->channels[$this->city])) {
            if (!isset($this->cityConfig[$this->city])) {
                if (!$this->discoverCity()) Http::notFound();
            }
        }


        /** @var AbstractOutput $outputClass */
        $this->outputClass = 'Peregrinus\\RKWrapper\\Output\\'.ucfirst(pathinfo($parts[1], PATHINFO_FILENAME)).'Output';
        if (!class_exists($this->outputClass)) Http::notFound();
    }

    public function getImport(): AbstractImport {
        if (isset($this->channels[$this->city]) && ($this->channels[$this->city]['isYouTube'] ?? false)) return new YoutubeImport($this->city, $this->channels[$this->city]['feed']);
        return new RkImport($this->city, $this->cityConfig[$this->city]);
    }

    public function getOutput()
    {
        $class = $this->outputClass;
        return new $class($this->getImport());
    }

    protected function discoverCity() {
        $urls = [
            'https://rk-solutions-stream.de/###CITY###/index-nopw.php',
            'https://rk-solutions-stream.de/###CITY###/index.php',
            'https://rk-solutions-streama.de/###CITY###/index-nopw.php',
            'https://rk-solutions-streama.de/###CITY###/index.php',
            'https://rk-solutions-streamb.de/###CITY###/index-nopw.php',
            'https://rk-solutions-streamb.de/###CITY###/index.php',
        ];

        foreach ($urls as $url) {
            $url = str_replace('###CITY###', $this->city, $url);
            if (Http::test($url)) {
                $this->cityConfig[$this->city] = $url;
                yaml_emit_file('data/cities.yaml', $this->cityConfig);
                return true;
            }
        }


        return false;
    }

}