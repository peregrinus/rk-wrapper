<?php

namespace Peregrinus\RKWrapper\Import;

use Carbon\Carbon;
use Goutte\Client;
use Peregrinus\RKWrapper\Http;
use Peregrinus\RKWrapper\StringHelper;

class RkImport extends AbstractImport
{

    public function scrape() {
        $crawler = $this->client->request('GET', $this->url);

        $table = $crawler->filter('table')->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                if ($i >= 5) {
                    return ($td->filter('a')->each(function ($a, $i){
                        $download = $a->attr('download');
                        if (!empty($download)) return 'https:'.str_replace('\/', '/', trim($download));
                        return '';
                    }));
                }
                return trim($td->text());
            });
        });

        $this->title = $crawler->filter('h2')->first()->text();

        for ($i=1; $i<count($table); $i++) {
            $row = $table[$i];
            $audioUrl = str_replace('.mp4', '.mp3', $row[7][0]);
            $record = [
                'video' => $row[7][0],
                'audio' => $audioUrl,
                'start' => Carbon::createFromFormat('d.m.Y H:i', $row[1].' '.$row[2])->setSecond(0),
                'end' => Carbon::createFromFormat('d.m.Y H:i', $row[1].' '.$row[3])->setSecond(0),
                'title' => $row[0].', '.$row[1].' '.$row[2],
                'length' => ['video' => StringHelper::convertToBytes($row[4]), 'audio' => $this->getRemoteFileSize($audioUrl)],
            ];
            $this->dates[] = Carbon::createFromFormat('d.m.Y H:i', $row[1].' '.$row[3])->setSecond(0);
            $record['seconds'] = $record['end']->diffInSeconds($record['start']);
            $record['duration'] = gmdate('H:i:s', $record['seconds']);
            $this->records[] = $record;
        }
    }

    public function getRemoteFileSize($url)
    {
        $headers = Http::head($url);
        return $headers['content-length'] ?? -1;
    }

}