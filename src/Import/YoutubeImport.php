<?php

namespace Peregrinus\RKWrapper\Import;

use Carbon\Carbon;

class YoutubeImport extends AbstractImport
{

    protected $config;

    public function __construct($city, $url)
    {
        $channels = yaml_parse_file('data/tv.yaml');
        $this->config = $channels[$city] ?? [];
        parent::__construct($city, $url);
    }


    public function scrape()
    {

        $xml = simplexml_load_string(file_get_contents($this->url));

        foreach ($xml->entry as $entry) {
            $pubDate = Carbon::parse((string)$entry->pubDate);
            if (preg_match('/(\d\d\.\d\d\.\d\d\d\d)/', (string)$entry->title[0], $tmp)) {
                $pubDate = Carbon::createFromFormat('d.m.Y', $tmp[1]);
            }
            $pubDate->setTime(0,0,0 );
            $videoId = $this->getVideoId((string)$entry->link[0]->attributes()->href);
            if ($pubDate <= Carbon::now()) {
                $record = [
                    'title' => $pubDate->format('d.m.'),
                    'description' => (string)$entry->title[0],
                    'video' => $videoId,
                    'dateString' => $pubDate->format('d.m.Y'),
                    'thumbnail' => 'https://i3.ytimg.com/vi/'.$videoId.'/hqdefault.jpg',
                    'start' => $pubDate,
                    'end' => $pubDate->copy()->addHour(1),
                ];

                $this->dates[] = $pubDate;
                $this->records[] = $record;
            }
        }
    }

    protected function getVideoId($url) {
        return strtr($url, ['https://www.youtube.com/watch?v=' => '', 'https://youtu.be/' => '']);
    }
}