<?php

namespace Peregrinus\RKWrapper\Import;

use Goutte\Client;

abstract class AbstractImport
{

    protected $city;
    protected $url;

    protected $title;

    protected $records = [];

    protected $dates = [];

    /** @var Client */
    protected $client = null;

    public function __construct($city, $url)
    {
        $this->city = $city;
        $this->url = $url;
        $this->client = new Client();
        $this->scrape();
    }

    abstract public function scrape();

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function getRecords()
    {
        return $this->records;
    }

    /**
     * @param array $records
     */
    public function setRecords($records)
    {
        $this->records = $records;
    }

    /**
     * @return array
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * @param array $dates
     */
    public function setDates($dates)
    {
        $this->dates = $dates;
    }

    public function getPubDate()
    {
        return max($this->dates)->format('r');
    }


}