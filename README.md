RK-Wrapper
==========

Backend for re-distributing recorded worship services to a variety of output formats (podcast, playlists, ...).
This is mainly (but not exclusively) a wrapper around RK Solutions' own backend, in order to watch their recordings 
on other devices like Android TV (via [Gottesdienste live](https://codeberg.org/peregrinus/gdtv)).

